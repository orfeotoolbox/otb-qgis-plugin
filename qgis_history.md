<!-- All these lines are extracted from QGis changelog with keyword otb -->

Nyall Dawson <nyall.dawson@gmail.com>	2023-11-25

    Drop OTB Processing provider

    As discussed in https://lists.osgeo.org/pipermail/qgis-developer/2023-November/066221.html

    This plugin needs to be ported to a 3rd party plugin by a motivated maintainer.

Nyall Dawson <nyall.dawson@gmail.com>	2023-11-10

    If user enabled OTB plugin without setting a path, don't show
    message box warnings when closing Settings dialog

    This is confusing for users who have accidentally enabled the plugin.
    If the path is empty, chances are the user isn't wanting to use
    the OTB provider and it's nicer not to show a big confusing
    message box warning to them.

Nyall Dawson <nyall.dawson@gmail.com>	2022-02-01

    In saga/grass/otb processing plugin tests, only load the provider
    being tested instead of every provider

Nyall Dawson <nyall.dawson@gmail.com>	2022-02-01

    When processing is initialized in external scripts, ensure the GRASS,
    SAGA and OTB providers are included by default

    Fixes #45935

Alexander Bruy <alexander.bruy@gmail.com>	2021-06-20

    tests for OTB provider

Alexander Bruy <alexander.bruy@gmail.com>	2021-06-15

    fix OTB folder handling

Alexander Bruy <alexander.bruy@gmail.com>	2021-06-03

    separate otb provider into a plugin

Julien Cabieces <julien.cabieces@oslandia.com>	2021-01-13

    [OTB] Fixes empty string parameters (#40957)

Julien Cabieces <julien.cabieces@oslandia.com>	2020-12-01

    Keep only vector layer file path when calling an OTB algorithm

Julien Cabieces <julien.cabieces@oslandia.com>	2020-12-01

    Fix otbalgorithm when several band are selected

Julien Cabieces <julien.cabieces@oslandia.com>	2020-11-16

    Fix QGIS/OTB interface for field parameters

Julien Cabieces <julien.cabieces@oslandia.com>	2020-11-10

    [processing][OTB] deal with parameter type Enum and Band

Julien Cabieces <julien.cabieces@oslandia.com>	2020-11-10

    [processing][OTB] Don't remove parameter if its value is 0

Julien Cabieces <julien.cabieces@oslandia.com>	2020-10-07

    [OTB] Fix path to OTB 7.1.0 release

Mickael Savinaud <mickael.savinaud@c-s.fr>	2020-07-01

    Fix OTB icon which is not render into QGIS Processing

Nyall Dawson <nyall.dawson@gmail.com>	2020-03-31

    Another fix for OTB widget

Nyall Dawson <nyall.dawson@gmail.com>	2020-03-31

    Adapt OTB widget use of non-stable API

    (this is a prime example of why the sooner we can drop all this python, the better...)

Cédric Traizet <cedric.traizet@c-s.fr>	2020-03-24

    OTB plugin modification for OTB 7.1

Cédric Traizet <cedric.traizet@c-s.fr>	2020-03-24

    Update OTB downloading link to version 7.1

Matthias Kuhn <matthias@opengis.ch>	2020-03-22

    Re-enable otb tests with stable download link of 7.0

Nyall Dawson <nyall.dawson@gmail.com>	2020-03-22

    Blacklist OTB tests on Travis: need updating for OTB 7.1

Nyall Dawson <nyall.dawson@gmail.com>	2020-03-22

    Try updating downloading links to OTB 7.1

Merge: 392532ee62d 562b5186ce2
Matthias Kuhn <matthias@opengis.ch>	2019-11-01

    Merge pull request #32574 from m-kuhn/fix_orfeo_link

    [travis] Fix link to OTB binaries

Matthias Kuhn <matthias@opengis.ch>	2019-11-01

    Fix link to OTB binaries

Jürgen E. Fischer <jef@norbit.de>	2019-10-21

    debian packaging: suggest otb-qgis

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-03-18

    [TEST] add more test on otb segmentation apps

    Even though not all errors are caught by these new tests, it could
    expose if otb is broken or if processing api is changed to adopt
    optional status of parameters at run-time.

    `alg.processAlgorithm()` is running and failing correctly.
    But `parameter.checkValueIsAcceptable()` and `alg.checkParameterValues()`
    aren't working as expected.

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-03-18

    fix #21373 #21374: handle update optional status based on user-input

    Parameters are marked required in descriptor file for certian
    applications but they are only required if a parent parameter has a
    "certain" value. So initial idea was to make all of those parameters
    optional from OTB part. So that qgis can work correctly and was a easy
    fix. But.. we (me and Antonie) decided not to take that route and
    found a better fix.

    `OtbParameterChoice` and its wrapper will update optional status of
    all sub-parameters depending on the value of a choice parameter.

    A test has been added to check this issue and will be available in
    next commit.

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-03-06

    Remove OtbSettings class and put constants for key names in OtbUtils

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-03-06

    This commits fixes encoding issue on windows.

    getWindowsCodePage is taken from Grass7Utils.py

    Instead of writing a cli_file at startup, provider now pass all
    required env_variables directly to subprocess.popen. This has known to
    cause issues when handling with windows path names. subprocess.Popen
    handles it correctly depending on platform

    Logging of output from otbalgorithm and updating progress bar is
    slightly updated.
    Algoirthm is now launched directly using otbApplicationLauncherCommandLine
    `encoding` (on windows) and env arguments passed to subprocess is
    logged in QgsMessageLog


Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-22

    update Otb Algorithm test to use Map Layer instance

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-22

    check for layer providers and authid

    OTB only supports gdal and ogr providers for now. Maybe memory
    provider can be easily supported using some conversion on the fly.
    For the moment, we can go with this method. IO Formats in OTB not
    using GDAL/OGR (LUM, ONERA) are not supported by QGis. Those can be
    treated as simple files.

    nyalldawson,  pointed that AUTHORITY id can have types not starting
    with 'EPSG:'. Current otb takes just EPSG number and run with it. The
    algorithm doesn't know what to with a number which is not EPSG because
    it uses Gdal's 'ImportFromEpsg' method AFAIR.

    QgsProecessing Exception is raised in both the above invalid cases.

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-21

    [test] new test for OtbChoiceWidget

    This can easily go into GuiTests.py but we tend to keep it out due to
    usage of create_from_metadata(). All widget in GuiTests uses
    create_from_class() which will not work for this special widget

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-21

    [test] a new test for otb algorithm that used crs

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-21

    Use parameterAs methods in OtbAlgorithm

    This will accept stuff other than string type if needed by a user.

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-17

    pass file names with quotes to otb

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-10

    [CI] fix travis test for OtbAlgorithms

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-08

    [CI] install OTB package for OtbAlgorithmsTest

    Package will be taken from official OTB repository and will be
    maintained by OTB team.

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-08

    [TEST] add test for OTB processing provider

    This includes yaml test like SAGA, GRASS and also test for loading
    OTB Algorithms

Rashad Kanavath <rashad.kanavath@c-s.fr>	2019-01-08

    [FEATURE] [needs-docs] integrate OTB provider to processing plugin

Merge: 95091d93423 f94f0d753c2
Alexander Bruy <alexander.bruy@gmail.com>	2017-02-02

    Merge pull request #4076 from volaya/remove_processing_providers

    [FEATURE][processing] removed otb and lidartools providers

    Tagged as feature to not forget mention this in changelog and docs

Víctor Olaya <volayaf@gmail.com>	2017-01-30

    [processing] removed otb tests

Víctor Olaya <volayaf@gmail.com>	2017-01-30

    [processing] removed otb and lidartools providers

Alexander Bruy <alexander.bruy@gmail.com>	2017-01-11

    [processing] update OTB's white- and blacklist. Drop obsolete 3.x
    versions and add latest 5.8.0

Alexander Bruy <alexander.bruy@gmail.com>	2017-01-11

    [processing] add new MultiImageSamplingRate algorithm from OTB 5.8.0

Alexander Bruy <alexander.bruy@gmail.com>	2017-01-10

    [processing] support OTB 5.8.0

Alexander Bruy <alexander.bruy@gmail.com>	2016-11-03

    [processing] add missed description files for OTB 5.4.0

Merge: fa3663907a7 8e663309c06
Nyall Dawson <nyall.dawson@gmail.com>	2016-10-31

    Merge pull request #3696 from nirvn/processing_otb_silent

    [processing] remove console error message when optional otb provider not installed

Mathieu Pellerin <nirvn.asia@gmail.com>	2016-10-31

    [processing] remove console error message when optional otb provider not installed

Merge: a3ebc7bdb64 df90e6b964f
Alexander Bruy <alexander.bruy@gmail.com>	2016-10-20

    Merge pull request #3646 from alexbruy/processing-otb-test

    [processing] add tests for OTB algorithms

Alexander Bruy <alexander.bruy@gmail.com>	2016-10-19

    [processing] minor formatting fixes to the OTB README

Manuel Grizonnet <manuel.grizonnet@cnes.fr>	2016-08-23

    TEST: add a test for otb conversion application (simpler than bandmath)

Alexander Bruy <alexander.bruy@gmail.com>	2016-10-20

    Install OTB in QGIS Travis CI script to be able to run OTB test

Alexander Bruy <alexander.bruy@gmail.com>	2016-10-20

    update readme for otb maintenance script

Manuel Grizonnet <manuel.grizonnet@cnes.fr>	2016-08-22

    TEST: add test of OTB BandMath application in processing

Merge: 3251eced699 df248dbe935
Alexander Bruy <alexander.bruy@gmail.com>	2016-08-23

    Merge branch 'otb_apps_export_improvment'

Merge: 3251eced699 ce0a2ae062d
Alexander Bruy <alexander.bruy@gmail.com>	2016-08-23

    Merge branch 'otb_apps_export_improvment' of https://github.com/grizonnetm/QGIS into otb_apps_export_improvment

Manuel Grizonnet <manuel.grizonnet@cnes.fr>	2016-08-22

    forget to commit xml descriptors for otb 5.6

Manuel Grizonnet <manuel.grizonnet@cnes.fr>	2016-08-22

    Update otb processing maintenance files to version 5.6.0

Manuel Grizonnet <manuel.grizonnet@cnes.fr>	2016-08-21

    ENH: update log info when parsing otb apps

Alexia Mondot <alexia.mondot@c-s.fr>	2016-08-17

    BUG NameError: name OTBSpecific_XMLcreation is not defined on exec

Alexia Mondot <alexia.mondot@c-s.fr>	2016-08-17

    ENH update for OTB 5.4

Alexander Bruy <alexander.bruy@gmail.com>	2016-04-10

    [processing] fix missed quotes in OTB provider (fix #14557)

Merge: 6800c8d9da6 716b17d216c
Alexander Bruy <alexander.bruy@gmail.com>	2016-02-27

    Merge pull request #2840 from vpoughon/processing_plugin_otb_fixes

    [processing] allow to specify OTB path manually even if default version detected


Victor Poughon <victor.poughon@cnes.fr>	2016-02-25

    Always enable OTB settings modification

Victor Poughon <victor.poughon@cnes.fr>	2016-02-25

    Fix error when otb lib path is not found

Alexander Bruy <alexander.bruy@gmail.com>	2016-02-02

    [processing] include OTB descriptions and help into CMakeLists.txt

Víctor Olaya <volayaf@gmail.com>	2016-01-15

    [processing] fixed otb version detection when no otb path is defined or found

Merge: df27c07d5f7 b68ee3d8f9c
Víctor Olaya <volayaf@gmail.com>	2016-01-15

    Merge pull request #2669 from volaya/otb_version

    [processing] improved version detection mechanism for OTB

Merge: 864e1921e6e b146c3edfc0
Víctor Olaya <volayaf@gmail.com>	2016-01-15

    Merge pull request #4 from alexbruy/otb-indentation

    fix indentation

Víctor Olaya <volayaf@gmail.com>	2016-01-13

    [processing] improved version detection mechanism for OTB

Víctor Olaya <volayaf@gmail.com>	2016-01-13

    [Processing] [OTB] fixed wrong command when alg help is not found

    fixes #14096

Alexia Mondot <alexia.mondot@c-s.fr>	2015-07-03

    [processing/otb]update xml and associated html docs

Alexia Mondot <alexia.mondot@c-s.fr>	2015-07-02

    [processing/otb]Add specific rule to load ComputeOGRLayersFeaturesStatistics

Alexia Mondot <alexia.mondot@c-s.fr>	2015-07-02

    [processing/otb]convert ListView in StringList for other applications than Radiometric Indices

Alexia Mondot <alexia.mondot@c-s.fr>	2015-06-25

    [processing/otb]divide into 2 functions

Alexia Mondot <alexia.mondot@c-s.fr>	2015-06-25

    [processing/otb]Add function to customize creation and loading of new applications

Alexia Mondot <alexia.mondot@c-s.fr>	2015-06-25

    [processing/otb]update lists for OTB 5.0

Alexia Mondot <alexia.mondot@c-s.fr>	2015-06-25

    [processing/otb]compatibility with new version of processing

Alexia Mondot <alexia.mondot@c-s.fr>	2015-06-25

    [processing/otb]remove unused code and imports

Víctor Olaya <volayaf@gmail.com>	2015-09-30

    [processing] prevent otb algorithms being run if otb not configured

    fixes #13215

Jürgen E. Fischer <jef@norbit.de>	2015-08-18

    processing: fix warning when loading otb algorithms

Merge: 6fbb2d6c260 b8c25251dfd
Víctor Olaya <volayaf@gmail.com>	2015-05-06

    Merge pull request #1910 from radosuav/obt_fix

    [Processing] Fix bug in reading ParameterMultipleInput in OTB algorithms.

Merge: 33d973f5e45 f94d92f014b
Alexander Bruy <alexander.bruy@gmail.com>	2015-04-09

    Merge pull request #1976 from radosuav/otb_double_quotes

    [processing] avoid consecutive quotes when calling OTB algorithms

radosuav <rmgu@dhi-gras.com>	2015-04-07

    [Processing] Avoid consecutive quotes when calling OTB algorithms.

radosuav <rmgu@dhi-gras.com>	2015-02-18

    [Processing] Fix bug in reading ParameterMultipleInput in OTB algorithms.

Alexander Bruy <alexander.bruy@gmail.com>	2015-01-16

    [processing] i18n support in grass, R, OTB and example providers

radosuav <rmgu@dhi-gras.com>	2014-10-06

    [Processing] Fix advanced parameter handling in OTB algorithms.

Rado Guzinski <rmgu@dhi-gras.com>	2014-07-22

    [Processing] Comment debuging statements in OTB which clutter up Processing log

Rado Guzinski <rmgu@dhi-gras.com>	2014-07-22

    [Processing] Hide some OTB params under Advanced button and improve handling of advanced parameters

Víctor Olaya <volayaf@gmail.com>	2014-06-12

    [processing] If SAGA/OTB exist as bundled app, do not allow manual configuration

Jürgen E. Fischer <jef@norbit.de>	2014-05-17

    processing: remove adsense from otb descriptions

Víctor Olaya <volayaf@gmail.com>	2014-03-31

    [processing] disable excesive info reporting from otb provider

Alexander Bruy <alexander.bruy@gmail.com>	2014-03-26

    [processing] move OTB output to INFO tab, fix deprecation warning

Merge: e2d01e2e158 7abca16d05a
Víctor Olaya <volayaf@gmail.com>	2014-03-25

    Merge pull request #1134 from CS-SI/otb_processing_update

    OTB processing update

Alexia Mondot <alexia.mondot@c-s.fr>	2014-01-17

    Update OTB processing modules

Víctor Olaya <volayaf@gmail.com>	2013-09-24

    [processing] improved saga/grass/otb installation checks

Víctor Olaya <volayaf@gmail.com>	2013-06-06

    [sextante] fixed issue with blank spaces in filenames in OTB

Víctor Olaya <volayaf@gmail.com>	2013-05-04

    [sextante] edited description of OTB orthorectification algorithm

Víctor Olaya <volayaf@gmail.com>	2013-05-02

    Adjustments in DEM parameters fro OTB algorithms

Víctor Olaya <volayaf@gmail.com>	2013-04-27

    [sextante] added OTB algorithms to simplified list

Víctor Olaya <volayaf@gmail.com>	2013-04-26

    [sextante] fixed problem in otb with filepaths containing whitespaces.

    Removed outdated otb descriptions

William Kyngesburye <kyngchaos@kyngchaos.com>	2013-04-07

    fix autodetect of saga & otb on OS X, also detect bundled grass, saga & otb

Víctor Olaya <volayaf@gmail.com>	2013-04-01

    [sextante] modified OTB description to adapt to 3.16

Víctor Olaya <volayaf@gmail.com>	2013-03-12

    [sextante]added new OTB description

Merge: f85d3ae71f5 79d5fb35b88
Víctor Olaya <volayaf@gmail.com>	2013-02-25

    Merge pull request #439 from mach0/master

    Added html help files to be installed in doc directory for OTB help

Víctor Olaya <volayaf@gmail.com>	2012-12-21

    minor fixes for otb and gdal, and edited some grass descriptions

Víctor Olaya <volayaf@gmail.com>	2012-11-10

    fixed problems with missing example scripts folder
    Restructured gdal tools (now gdal/ogr)
    Added new OTB segmentation algos

Víctor Olaya <volayaf@gmail.com>	2012-10-17

    Several patches contributed by Rado Guzinski:
    -New OTB descriptions an help
    -Update OTB descriptions
    -More responsive MultipleInputPanel.py
    -Minor bug fixes

cpolymeris@gmail.com <cpolymeris@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-08-07

    Log console output (SAGA; OTB; GRASS).


    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@339 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

cpolymeris@gmail.com <cpolymeris@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-08-06

    OTB and GRASS commandline to log.


    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@335 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

cpolymeris@gmail.com <cpolymeris@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-06-22

    OTB ROI: apply to vectors


    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@260 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

cpolymeris@gmail.com <cpolymeris@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-06-22

    OTB ROI: support multiple input rasters


    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@259 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

cpolymeris@gmail.com <cpolymeris@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-06-19

    OTB helper commands log.


    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@255 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

cpolymeris@gmail.com <cpolymeris@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-06-19

    OTB extents. Test implementation.


    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@254 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

cpolymeris@gmail.com <cpolymeris@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-06-16

    otb: region of interest.


    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@249 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

Víctor Olaya <volayaf@gmail.com>	2012-06-08

    fixed minor bug in in OTB provider

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@232 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

Víctor Olaya <volayaf@gmail.com>	2012-06-07

    fixed problem when opening OTB descriptions

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@231 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-19

    [otb] fix OutputFile declaration and clean up when no default value available

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@130 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-19

    [otb] recognize linux default paths to avoid configuration

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@128 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-19

    [otb] set default value for ParameterSelection

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@127 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-19

    [otb] use new ParameterFile and OutputFile type

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@126 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-19

    [otb] more user friendly naming of algorithms

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@125 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-19

    [core] missing import, lost grass and otb on linux

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@124 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-19

    [otb] classify application with their tags

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@123 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d


Víctor Olaya <volayaf@gmail.com>	2012-04-16

    fixed indentation in OTBAlgorithm

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@105 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

Víctor Olaya <volayaf@gmail.com>	2012-04-16

    fixed indentation in OTBAlgorithm

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@103 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d


julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-14

    [otb] add options for saving dem options

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@98 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d


Tim Sutton <tim@linfiniti.com>	2012-04-14

    Inital implementation of test submission to CDash using OTB Dash server. Use make Experimental to submit your tests.

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-14

    [otb] fix choice parameter

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@97 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-14

    [otb] remove last empty choice

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@96 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-14

    [otb] handle not filled string parameters

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@95 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] update description after previous commits

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@91 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] need to specify vector layer type

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@90 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] None not allowed for default value

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@89 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] add multiple input support

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@88 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] add quotes around command parameter

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@87 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] use parameter name instead of description

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@85 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    fix command launching for otb and gdal

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@83 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] fix application descriptors

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@82 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] fix application descriptor generator for Mandatory/optional management

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@81 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] add .gitignore file

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@79 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] add description for otb modules

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@78 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] add helper script

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@77 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

julien.malik@gmail.com <julien.malik@gmail.com@881b9c09-3ef8-f3c2-ec3d-21d735c97f4d>	2012-04-13

    [otb] fix command launching for linux

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@76 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

Víctor Olaya <volayaf@gmail.com>	2012-04-13

    Made OTB configurable also in linux
    Added extent parameter

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@74 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

Víctor Olaya <volayaf@gmail.com>	2012-04-12

    Added first working OTB algorithm
    Added file selector for vector and raster layers
    fixed bug with numbers and string in modeler dialog
    added RenderingStyleFilePanel

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@73 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

Víctor Olaya <volayaf@gmail.com>	2012-04-11

    relocated otb folder (wrongly imported in the previous commit)

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@71 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d

Víctor Olaya <volayaf@gmail.com>	2012-04-11

    Did some R and GRASS testing and debugging
    Added the OTB folder with a skeleton of OTB alg provider

    git-svn-id: http://sextante.googlecode.com/svn/trunk/soft/bindings/qgis-plugin@70 881b9c09-3ef8-f3c2-ec3d-21d735c97f4d
