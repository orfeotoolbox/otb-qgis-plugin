# ![logo](resources/providerOtb.svg "otb") OrfeoToolbox Provider

[![pipeline status](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb-qgis-plugin/badges/main/pipeline.svg)](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb-qgis-plugin/commits/main)
[![QGIS.org](https://img.shields.io/badge/QGIS.org-published-green)](https://plugins.qgis.org/plugins/orfeoToolbox_provider/)
![Downloads](https://img.shields.io/badge/dynamic/json?formatter=metric&color=grey&label=Downloads&query=%24.OrfeoToolbox_Provider.downloads&url=https://raw.githubusercontent.com/Mariosmsk/qgis-plugins-downloads/main/data/plugins.json)

This plugin expose [OTB applications](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb) in QGis viewer. Since QGis 3.36, the OTB team maintains this plugin (see [qgis 3.36 breaking changes](https://qgis.org/en/site/forusers/visualchangelog336/index.html#breaking-changes)). This code is originally a copy of folder "python/plugins/otbprovider" at commit e9dd076 of QGis repo.

## Installation

There is two installation methods:
- Using QGis plugin catalog
- Using a zip archive you created with qgis-plugin-ci (you get the latest features)

### Look for otb plugin
This plugin is available in QGis plugin's catalog. You can access from menu Extensions->Install->Non-Installed, and use the search box with "OrfeoToobox" keyword. A more visual procedure is available [on QGis website](https://docs.qgis.org/latest/en/docs/user_manual/plugins/plugins.html).

### Create a zip

First download this project, then install qgis-plugin-ci.

```bash
pip install qgis-plugin-ci
```

If the following instruction does not work on your OS, you may find instructions at [qgis-plugin-ci repo](https://github.com/opengisch/qgis-plugin-ci/tree/master).

When qgis-plugin-ci installed, you only need the following command to create an archive usable by QGis:

```bash
# we assume you are at the root of the project
# use the -c package option if you have uncommited changes
qgis-plugin-ci package <major>.<min>.<patch>
```
Then install you zip on QGis, Menu Extensions->Install->From Zip. Or follow this procedure [at "Install from zip" section](https://docs.qgis.org/latest/en/docs/user_manual/plugins/plugins.html).

## Usage

Once installed, there are some settings to respect before using otb application inside QGis. On QGis "Preferences" Menu->Options, a popup opens. On the left side, go to Processing section at the bottom (see the following picture). There should be OTB in the provider list.

![Processing Providers](resources/processing_providers.png)

The *OTB_FOLDER* and *OTB_APPLICATION_FOLDER* are mandatories:
- *OTB_FOLDER* is the path to the root of your otb installation
- *OTB_APPLICATION_FOLDER* is where the otb binaries are installed. Most of the time at *OTB_FOLDER*/lib/otb/applications

Once completed, otb applications should be available in Processing toolbox part of QGis GUI.

## Usefull links

- QGis doc to create plugin https://docs.qgis.org/testing/en/docs/pyqgis_developer_cookbook/plugins/index.html
- qgis-plugin-ci documentation to easily build this plugin https://opengisch.github.io/qgis-plugin-ci/index.html
- plugin management on qgis website https://plugins.qgis.org/plugins/my 
- osgeo account management https://www.osgeo.org/community/getting-started-osgeo/osgeo_userid/
